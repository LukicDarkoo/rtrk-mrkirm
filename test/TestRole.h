#ifndef TESTROLE_H
#define TESTROLE_H

#include <cppunit/extensions/HelperMacros.h>
#include "../src/TunnelFileTransfer.h"

class TestRole : public CPPUNIT_NS::TestFixture
{
  CPPUNIT_TEST_SUITE( TestRole );
  CPPUNIT_TEST( basic );
  CPPUNIT_TEST_SUITE_END();

protected:
  TunnelFileTransfer* tunnel1;
  TunnelFileTransfer* tunnel2;

public:
  void setUp();

protected:
  void basic();
};


#endif
