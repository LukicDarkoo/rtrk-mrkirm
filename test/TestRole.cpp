#include "TestRole.h"

#include <QThread>
#include <QTimer>

CPPUNIT_TEST_SUITE_REGISTRATION( TestRole );

void TestRole::basic() {
    CPPUNIT_ASSERT( tunnel1->getRole() == TunnelFileTransfer::SERVER );
}

void TestRole::setUp() {
    auto thread = new QThread();
    QObject::connect(thread, &QThread::started, [&](){
        QThread::sleep(1);
        tunnel2 = new TunnelFileTransfer();
        tunnel2->start();
    });
    thread->start();

    tunnel1 = new TunnelFileTransfer();
    tunnel1->start();
}
