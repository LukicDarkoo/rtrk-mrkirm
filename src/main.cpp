#include "mainwindow.h"
#include <QApplication>
#include <QFileDialog>
#include <iostream>
#include <QThread> // msleep()
#include <QCoreApplication>



#include "TunnelFileTransfer.h"

// #define GUI

TunnelFileTransfer* transporter;

int main(int argc, char *argv[]) {
#ifdef GUI
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
#else
    QCoreApplication a(argc, argv);

    transporter = new TunnelFileTransfer();
    transporter->start();
    QThread::sleep(1);
    if (transporter->getRole() == TunnelFileTransfer::SERVER) {
        transporter->upload("for_upload.jpg");
    }

    return a.exec();
#endif
}
