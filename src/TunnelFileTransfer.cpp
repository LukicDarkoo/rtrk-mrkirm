#include "TunnelFileTransfer.h"
#include <fstream>
#include <QThread>

TunnelFileTransfer::TunnelFileTransfer(QObject *parent) : QObject(parent) {
    this->setState(State::CHOOSING_ROLE);
    this->role = Role::SERVER;
}

void TunnelFileTransfer::start(QString address, quint16 port) {
    QHostAddress hostAddress(address);
    this->socket = new QTcpSocket();

    this->socket->connectToHost(hostAddress, port);
    if (this->socket->waitForConnected(1000)) {
        this->role = Role::CLIENT;
        qDebug() << "TYPE: Client";
    }
    else {
        delete this->socket;
        this->server.listen(QHostAddress::Any, port);
        this->role = Role::SERVER;
        qDebug() << "TYPE: Server";
        this->server.waitForNewConnection(1000000);
        this->socket = this->server.nextPendingConnection();
    }

    this->setState(State::IDLE);

    connect(this->socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(this->socket, SIGNAL(connected()),this, SLOT(onConnect()));
    connect(this->socket, SIGNAL(disconnected()), this, SLOT(onDisconnect()));
    connect(this->socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
    connect(this->socket, SIGNAL(bytesWritten(qint64)), this, SLOT(onBytesWritten(qint64)));
}

void TunnelFileTransfer::onConnect() {
    qDebug() << "Connected";
}

void TunnelFileTransfer::onDisconnect() {
    qDebug() << "Disconnected";
}

void TunnelFileTransfer::onBytesWritten(qint64 bytes) {
    qDebug() << "Bytes written: " << bytes;
}

void TunnelFileTransfer::onError(QAbstractSocket::SocketError error) {
    qDebug() << "Error: " << this->socket->errorString();
}

void TunnelFileTransfer::onReadyRead() {
    switch(this->state) {
        case State::IDLE: {
            Packet* packet = new Packet(socket->readAll().data());
            char* payload = packet->getPayload();

            this->packetsToReceive = 0;
            this->packetsToReceive |= (payload[0] << 24);
            this->packetsToReceive |= (payload[1] << 16);
            this->packetsToReceive |= (payload[2] << 8);
            this->packetsToReceive |= payload[3];

            qDebug() << "File consists of" << this->packetsToReceive << "packets";

            this->setState(State::RECEIVING_FILE);
            of = new std::ofstream("downloaded.jpg", std::ofstream::binary);

            this->packets.clear(); }
            break;

        case State::RECEIVING_FILE: {
            Packet* packet = new Packet(socket->readAll().data());

            this->setState(State::RECEIVING_PACKET);

            of->write(packet->getPayload(), PACKET_PAYLOAD_SIZE);

            qDebug() << "Receiving packet[" << packet->getIndex() << "]";

            if (packet->getIndex() < this->packetsToReceive) {
                this->setState(State::RECEIVING_FILE);
            } else {
                qDebug() << "File is downloaded & saved";
                this->setState(State::IDLE);
                of->close();
                delete of;
            } }
            break;
    }
}

void TunnelFileTransfer::upload(char* filename) {
    char buffer[PACKET_PAYLOAD_SIZE];
    quint32 index = 0;
    quint8 destinationIp[4] = { 127, 0, 0, 1 };
    quint8 sourceIp[4] = { 127, 0, 0, 1 };

    this->setState(State::SENDING_FILE);

    // Open file
    std::ifstream input(filename, std::ios::binary);
    if (!input) {
        qDebug() << "ERROR: Cannot read file";
        return;
    }

    // Read packets
    while (input.read(buffer, PACKET_PAYLOAD_SIZE).eof() == false) {
        Packet* packet = new Packet(buffer,
                      ++index,
                      input.gcount(),
                      destinationIp,
                      sourceIp
                );
        packet->getIndex();
        packets.push_back(packet);
    }

    // Send start packet
    buffer[0] = (index >> 24) & 0xff;
    buffer[1] = (index >> 16) & 0xff;
    buffer[2] = (index >> 8) & 0xff;
    buffer[3] = index & 0xff;
    Packet* packet = new Packet(buffer,
          0,
          4,
          destinationIp,
          sourceIp
    );
    this->sendPacket(packet);
    delete packet;

    // Close file
    input.close();

    this->setState(State::SENDING_PACKET);

    //
    for (int i = 0; i < index; i++) {
        this->sendPacket(this->packets[i]);
        QThread::msleep(100);
    }

    qDebug() << "File is packetized";
}

void TunnelFileTransfer::sendPacket(Packet* packet) {
    this->socket->write((const char *)packet, sizeof(Packet));
    this->socket->flush();
    qDebug() << "Packet[" << packet->getIndex() << "] sent";
}

TunnelFileTransfer::~TunnelFileTransfer() {
    qDebug() << "~TunnelFileTransfer";

    // Delete packets
    for (size_t i = 0; i < this->packets.size(); i++) {
        delete this->packets[i];
    }

    // Delete socket
    delete this->socket;
}

void TunnelFileTransfer::setState(State state) {
    this->state = state;
    qDebug() << "State:" << state;
}
