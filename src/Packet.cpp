#include "Packet.h"
#include <QDebug>

Packet::Packet(
    char* buffer,
    quint32 index,
    quint16 length,
    quint8 destinationIp[4],
    quint8 sourceIp[4]) {

    this->data[PACKET_INDEX_INDEX] = (index >> 24) & 0xFF;
    this->data[PACKET_INDEX_INDEX + 1] = (index >> 16) & 0xFF;
    this->data[PACKET_INDEX_INDEX + 2] = (index >> 8) & 0xFF;
    this->data[PACKET_INDEX_INDEX + 3] = index & 0xFF;

    this->data[PACKET_LENGTH_INDEX] = (length >> 8) & 0xFF;
    this->data[PACKET_LENGTH_INDEX + 1] = length & 0xFF;

    this->data[PACKET_DESTINATION_IP_INDEX] = destinationIp[0];
    this->data[PACKET_DESTINATION_IP_INDEX + 1] = destinationIp[1];
    this->data[PACKET_DESTINATION_IP_INDEX + 2] = destinationIp[2];
    this->data[PACKET_DESTINATION_IP_INDEX + 3] = destinationIp[3];

    this->data[PACKET_SOURCE_IP_INDEX] = sourceIp[0];
    this->data[PACKET_SOURCE_IP_INDEX + 1] = sourceIp[1];
    this->data[PACKET_SOURCE_IP_INDEX + 2] = sourceIp[2];
    this->data[PACKET_SOURCE_IP_INDEX + 3] = sourceIp[3];

    memcpy(this->data + PACKET_PAYLOAD_INDEX, buffer, PACKET_PAYLOAD_SIZE);
}

Packet::Packet(char* data) {
    memcpy(this->data, data, PACKET_DATA_SIZE);
}

Packet::~Packet() {
    // Destructor
}

quint32 Packet::getIndex() {
    quint32 index = (this->data[PACKET_INDEX_INDEX] << 24) & 0xff000000;
    index |= (this->data[PACKET_INDEX_INDEX + 1] << 16) & 0xff0000;
    index |= (this->data[PACKET_INDEX_INDEX + 2] << 8) & 0xff00;
    index |= this->data[PACKET_INDEX_INDEX + 3] & 0xff;

    return index;
}

char* Packet::getData() {
    return this->data;
}

char* Packet::getPayload() {
    return (this->data + PACKET_PAYLOAD_INDEX);
}
