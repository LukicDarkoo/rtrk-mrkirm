#ifndef PACKET_H
#define PACKET_H

#define PACKET_PAYLOAD_SIZE 1500

// Emulated IP segment
#define PACKET_DESTINATION_IP_INDEX     0
#define PACKET_SOURCE_IP_INDEX          4
#define PACKET_LENGTH_INDEX             8

// Extended for file transfer
#define PACKET_INDEX_INDEX              10

// Payload
#define PACKET_PAYLOAD_INDEX            14

#define PACKET_DATA_SIZE                PACKET_PAYLOAD_SIZE + PACKET_PAYLOAD_INDEX + 1

#include <QObject>

class Packet {
public:
    Packet(char* buffer,
           quint32 index,
           quint16 length,
           quint8 destinationIp[4],
           quint8 sourceIp[4]);
    Packet(char* data);
    ~Packet();

    quint32 getIndex();
    char* getData();
    char* getPayload();
private:
    char data[PACKET_DATA_SIZE];
};

#endif // PACKET_H
