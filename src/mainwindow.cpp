#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    connect(ui->pushButton, SIGNAL(clicked(bool)), this, SLOT(chooseFile(void)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::chooseFile() {
    QFileDialog::getOpenFileName(this, "Open a file");

}
