#ifndef TUNNELFILETRANSFER_H
#define TUNNELFILETRANSFER_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>
#include <QVector>
#include "Packet.h"

class TunnelFileTransfer : public QObject
{
    Q_OBJECT
public:
    explicit TunnelFileTransfer(QObject *parent = 0);
    ~TunnelFileTransfer();
    void start(QString address = "127.0.0.1", quint16 port = 1337);
    void upload(char* filename);


    enum State {
        CHOOSING_ROLE,
        IDLE,
        SENDING_FILE,
        SENDING_PACKET,
        RECEIVING_FILE,
        RECEIVING_PACKET
    };

    enum Role {
        SERVER = 0,
        CLIENT
    };

    Role getRole() { return this->role; }
signals:


private slots:
    void onError(QAbstractSocket::SocketError error);
    void onBytesWritten(qint64 bytes);
    void onReadyRead();
    void onDisconnect();
    void onConnect();

private:
    Role role;
    State state;
    void setState(State state);
    uint32_t packetsToReceive;

    QTcpSocket* socket;
    QTcpServer server;
    std::ofstream* of;

    QVector<Packet*> packets;
    void sendPacket(Packet* packet);
};

#endif // TUNNELFILETRANSFER_H
